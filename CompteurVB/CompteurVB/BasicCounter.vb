﻿Imports CompteurLib

Public Class BasicCounter
    Dim valcompteur As Integer


    Private Sub btn_decr_Click(sender As Object, e As EventArgs) Handles btn_decr.Click
        valcompteur = Compteur.Decr_Compteur(valcompteur)
        lbl_valCompteur.Text = valcompteur
    End Sub

    Private Sub btn_incr_Click(sender As Object, e As EventArgs) Handles btn_incr.Click
        valcompteur = Compteur.Incr_Compteur(valcompteur)
        lbl_valCompteur.Text = valcompteur
    End Sub

    Private Sub btn_RAZ_Click(sender As Object, e As EventArgs) Handles btn_RAZ.Click
        valcompteur = Compteur.RAZ_Compteur(valcompteur)
        lbl_valCompteur.Text = valcompteur
    End Sub
End Class
