﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_incr = New System.Windows.Forms.Button()
        Me.btn_decr = New System.Windows.Forms.Button()
        Me.btn_RAZ = New System.Windows.Forms.Button()
        Me.lbl_total = New System.Windows.Forms.Label()
        Me.lbl_valCompteur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_incr
        '
        Me.btn_incr.Location = New System.Drawing.Point(96, 44)
        Me.btn_incr.Name = "btn_incr"
        Me.btn_incr.Size = New System.Drawing.Size(30, 23)
        Me.btn_incr.TabIndex = 0
        Me.btn_incr.Text = "+"
        Me.btn_incr.UseVisualStyleBackColor = True
        '
        'btn_decr
        '
        Me.btn_decr.Location = New System.Drawing.Point(12, 44)
        Me.btn_decr.Name = "btn_decr"
        Me.btn_decr.Size = New System.Drawing.Size(30, 23)
        Me.btn_decr.TabIndex = 1
        Me.btn_decr.Text = "-"
        Me.btn_decr.UseVisualStyleBackColor = True
        '
        'btn_RAZ
        '
        Me.btn_RAZ.Location = New System.Drawing.Point(33, 73)
        Me.btn_RAZ.Name = "btn_RAZ"
        Me.btn_RAZ.Size = New System.Drawing.Size(75, 23)
        Me.btn_RAZ.TabIndex = 2
        Me.btn_RAZ.Text = "RAZ"
        Me.btn_RAZ.UseVisualStyleBackColor = True
        '
        'lbl_total
        '
        Me.lbl_total.AutoSize = True
        Me.lbl_total.Location = New System.Drawing.Point(55, 22)
        Me.lbl_total.Name = "lbl_total"
        Me.lbl_total.Size = New System.Drawing.Size(31, 13)
        Me.lbl_total.TabIndex = 3
        Me.lbl_total.Text = "Total"
        '
        'lbl_valCompteur
        '
        Me.lbl_valCompteur.AutoSize = True
        Me.lbl_valCompteur.Location = New System.Drawing.Point(64, 49)
        Me.lbl_valCompteur.Name = "lbl_valCompteur"
        Me.lbl_valCompteur.Size = New System.Drawing.Size(13, 13)
        Me.lbl_valCompteur.TabIndex = 4
        Me.lbl_valCompteur.Text = "0"
        '
        'BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(138, 122)
        Me.Controls.Add(Me.lbl_valCompteur)
        Me.Controls.Add(Me.lbl_total)
        Me.Controls.Add(Me.btn_RAZ)
        Me.Controls.Add(Me.btn_decr)
        Me.Controls.Add(Me.btn_incr)
        Me.Name = "BasicCounter"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_incr As Button
    Friend WithEvents btn_decr As Button
    Friend WithEvents btn_RAZ As Button
    Friend WithEvents lbl_total As Label
    Friend WithEvents lbl_valCompteur As Label
End Class
