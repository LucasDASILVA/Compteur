﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompteurLib
{
    public class Compteur
    {
        private int val_compteur = 0;

        static public int Incr_Compteur(int val_compteur)
        {
            val_compteur = val_compteur + 1;
            return val_compteur;
        }

        static public int Decr_Compteur(int val_compteur)
        {
            val_compteur = val_compteur - 1;
            return val_compteur;
        }

        static public int RAZ_Compteur(int val_compteur)
        {
            val_compteur = 0;
            return val_compteur;
        }

    }
}
