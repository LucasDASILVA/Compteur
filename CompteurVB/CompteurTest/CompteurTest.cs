﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompteurLib;

namespace CompteurTest
{
    [TestClass]
    public class CompteurTest
    {
        [TestMethod]
         public int Incr_Compteur()
         {
             Assert.AreEqual(3, Compteur.Incr_Compteur(2));
         }

         public int Decr_Compteur()
         {
             Assert.AreEqual(3, Compteur.Decr_Compteur(4));
         }

         public int RAZ_Compteur()
         {
             Assert.AreEqual(0, Compteur.Incr_Compteur(2));
         }
        
    }
}
