﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_decrementation = New System.Windows.Forms.Button()
        Me.btn_incrementation = New System.Windows.Forms.Button()
        Me.lbl_Total = New System.Windows.Forms.Label()
        Me.btn_RAZ = New System.Windows.Forms.Button()
        Me.lbl_valeurCompteur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_decrementation
        '
        Me.btn_decrementation.Location = New System.Drawing.Point(47, 45)
        Me.btn_decrementation.Name = "btn_decrementation"
        Me.btn_decrementation.Size = New System.Drawing.Size(37, 23)
        Me.btn_decrementation.TabIndex = 0
        Me.btn_decrementation.Text = "-"
        Me.btn_decrementation.UseVisualStyleBackColor = True
        '
        'btn_incrementation
        '
        Me.btn_incrementation.Location = New System.Drawing.Point(154, 45)
        Me.btn_incrementation.Name = "btn_incrementation"
        Me.btn_incrementation.Size = New System.Drawing.Size(37, 23)
        Me.btn_incrementation.TabIndex = 1
        Me.btn_incrementation.Text = "+"
        Me.btn_incrementation.UseVisualStyleBackColor = True
        '
        'lbl_Total
        '
        Me.lbl_Total.AutoSize = True
        Me.lbl_Total.Location = New System.Drawing.Point(99, 32)
        Me.lbl_Total.Name = "lbl_Total"
        Me.lbl_Total.Size = New System.Drawing.Size(31, 13)
        Me.lbl_Total.TabIndex = 2
        Me.lbl_Total.Text = "Total"
        '
        'btn_RAZ
        '
        Me.btn_RAZ.Location = New System.Drawing.Point(81, 74)
        Me.btn_RAZ.Name = "btn_RAZ"
        Me.btn_RAZ.Size = New System.Drawing.Size(75, 23)
        Me.btn_RAZ.TabIndex = 3
        Me.btn_RAZ.Text = "RAZ"
        Me.btn_RAZ.UseVisualStyleBackColor = True
        '
        'lbl_valeurCompteur
        '
        Me.lbl_valeurCompteur.AutoSize = True
        Me.lbl_valeurCompteur.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_valeurCompteur.Location = New System.Drawing.Point(106, 45)
        Me.lbl_valeurCompteur.Name = "lbl_valeurCompteur"
        Me.lbl_valeurCompteur.Size = New System.Drawing.Size(24, 26)
        Me.lbl_valeurCompteur.TabIndex = 4
        Me.lbl_valeurCompteur.Text = "0"
        '
        'BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(240, 163)
        Me.Controls.Add(Me.lbl_valeurCompteur)
        Me.Controls.Add(Me.btn_RAZ)
        Me.Controls.Add(Me.lbl_Total)
        Me.Controls.Add(Me.btn_incrementation)
        Me.Controls.Add(Me.btn_decrementation)
        Me.Name = "BasicCounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_decrementation As Button
    Friend WithEvents btn_incrementation As Button
    Friend WithEvents lbl_Total As Label
    Friend WithEvents btn_RAZ As Button
    Friend WithEvents lbl_valeurCompteur As Label
End Class
