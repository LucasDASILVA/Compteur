﻿Imports CompteurLib

Public Class BasicCounter

    Public Compteur1 As Compteur = New Compteur(0)

    Private Sub btn_decrementation_Click(sender As Object, e As EventArgs) Handles btn_decrementation.Click
        lbl_valeurCompteur.Text = Compteur1.Decrementation()
    End Sub

    Private Sub btn_RAZ_Click(sender As Object, e As EventArgs) Handles btn_RAZ.Click
        lbl_valeurCompteur.Text = Compteur1.RAZ()
    End Sub

    Private Sub btn_incrementation_Click(sender As Object, e As EventArgs) Handles btn_incrementation.Click
        lbl_valeurCompteur.Text = Compteur1.Incrementation()
    End Sub
End Class
