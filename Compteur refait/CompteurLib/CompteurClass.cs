﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompteurLib
{
    public class Compteur
    {
        private int valeurCompteur;

        public Compteur(int valeurCompteur)
        {
            this.valeurCompteur = valeurCompteur;
        }

        public int Incrementation()
        {
            valeurCompteur = valeurCompteur + 1;
            return valeurCompteur;
        }

        public int Decrementation()
        {
            valeurCompteur = valeurCompteur - 1;
            return valeurCompteur;
        }

        public int RAZ()
        {
            valeurCompteur = 0;
            return valeurCompteur;
        }
    }
}
