using System;
using Xunit;
using CompteurLib;

namespace CompteurTest
{
    public class Test
    {
        [Fact]
        public void TestIncrementation()
        {
            Compteur Compteur1 = new Compteur(0);
            Assert.Equal(1, Compteur1.Incrementation());
            Compteur Compteur2 = new Compteur(4);
            Assert.Equal(5, Compteur2.Incrementation());

        }

        [Fact]
        public void TestDecrementation()
        {
            Compteur Compteur1 = new Compteur(3);
            Assert.Equal(2, Compteur1.Decrementation());
        }

        [Fact]
        public void TestRAZ()
        {
            Compteur Compteur1 = new Compteur(0);
            Assert.Equal(0, Compteur1.RAZ());
        }
    }
}
